package application;
	
import java.io.IOException;


import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import views.MainView2Controller;
import views.MainView3Controller;
import views.MainView4Controller;
import views.MainView5Controller;
import views.MainViewController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


/**
 * @author Kanes K. | 56070501004
 *
 */
public class Main extends Application {
	private Stage primaryStage;
	
	/*
	 * Initialize View Controller - Different in each view 
	 * Change controller class when change view
	 */
	private MainView5Controller mainViewController;
	
	/* init javaFX View (Override JavaFX Method which auto-loaded when run) */
	@Override
	public void start(Stage primaryStage) {
		
		// Load OpenCV Library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		// JavaFX View variable
		this.primaryStage = primaryStage;
		
		try {
			
			/* Load JavaFX Main View */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("../views/MainView5.fxml")); //path to view
			Parent root = loader.load();
			Scene scene = new Scene(root);
			primaryStage.setTitle("CPE489_Quiz1");
	        primaryStage.setScene(scene);
	        primaryStage.show();
	        
	        /* Call View Controller Function to load Image, calculate its Histogram, Gaussian, etc. */
	        String imgSrc = "resources/q5.png"; // path to image
	        mainViewController = loader.getController();
	       // mainViewController.loadImage(imgSrc); // for Q1 - 4
	       // mainViewController.calculateHistogram(imgSrc); // for Q1
	        mainViewController.gaussianFilter(imgSrc, 13, 2); // for Q5
	        
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		launch(args);
	}
}
