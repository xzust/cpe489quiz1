package views;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Kanes K. | 56070501004
 *
 */
public class MainView4Controller implements Initializable {

	/* Variables that bind with FXML Objects */
	@FXML
	private ImageView originalImg;
	@FXML
	private ImageView gaussian7xImg;
	@FXML
	private ImageView gaussian13xImg;
	
	/* Convert OpenCV Mat to JavaFX Image Object */
	public Image convertCVMatToImage(Mat mat) {
		MatOfByte buffer = new MatOfByte();
		Highgui.imencode(".bmp", mat, buffer);
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}

	/* Load image from Image Source then display in JavaFX ImageView Object */
	public void loadImage(String imgSrc) {
		
		/* read original image and display */
		Mat mat = Highgui.imread(imgSrc, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		Image image = convertCVMatToImage(mat);
		originalImg.setImage(image);
		
		/* 
		 * Initialize OpenCV Imgproc Object in order to using Gaussian Filter 
		 * mat2, mat3 as destination of gaussian filter
		 * mat2 using gaussian filter of 7x7 with sigma^2 = 2 -> sigma = sqrt(2)
		 * mat3 using gaussian filter of 13x13 with sigma = 2
		*/
		Imgproc imgproc = new Imgproc();
		Mat mat2 = new Mat();
		Mat mat3 = new Mat();
		Size size7x7 = new Size(7,7);
		Size size13x13 = new Size(13,13);
		imgproc.GaussianBlur(mat, mat2, size7x7, Math.sqrt(2));
		imgproc.GaussianBlur(mat, mat3, size13x13, 2);
		
		/* Convert OpenCV Mat into JavaFX image and display */
		Image image2 = convertCVMatToImage(mat2);
		Image image3 = convertCVMatToImage(mat3);
		gaussian7xImg.setImage(image2);
		gaussian13xImg.setImage(image3);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

}
