package views;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.highgui.Highgui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Kanes K. | 56070501004
 *
 */
public class MainView2Controller implements Initializable {

	/* Variables that bind with FXML Objects */
	@FXML
	private ImageView originalImg;
	@FXML
	private ImageView thresholdImg;
	@FXML
	private Label lblThreshold;
	
	/* Convert OpenCV Mat to JavaFX Image Object */
	public Image convertCVMatToImage(Mat mat) {
		MatOfByte buffer = new MatOfByte();
		Highgui.imencode(".bmp", mat, buffer);
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}

	/* Load image from Image Source then display in JavaFX ImageView Object */
	public void loadImage(String imgSrc) {
		
		/* read original image and display */
		Mat mat = Highgui.imread(imgSrc, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		Image image = convertCVMatToImage(mat);
		originalImg.setImage(image);

		/* initial estimate T */
		int sum = 0;
		for (int i = 0; i < mat.rows(); i++) {
			for (int j = 0; j < mat.cols(); j++) {
				double data[] = mat.get(i, j);
				sum = sum + (int) data[0];
			}
		}
		int T = Math.round(sum / (mat.rows() * mat.cols()));

		/* Automatic Threshold Algorithm */
		int prevMean1 = 0, prevMean2 = 0;
		int mean1 = 0, mean2 = 0;
		do {
			/* Save last mean value before new calculation */
			prevMean1 = mean1;
			prevMean2 = mean2;
			
			/* Partition image and calculate sum, n for R1, R2 */
			int sum1 = 0, sum2 = 0;
			int n1 = 0, n2 = 0;
			for (int i = 0; i < mat.rows(); i++) {
				for (int j = 0; j < mat.cols(); j++) {
					double data[] = mat.get(i, j);
					if (data[0] > T) {
						sum1 = sum1 + (int) data[0];
						n1++;
					}
					else {
						sum2 = sum2 + (int) data[0];
						n2++;
					}
				}
			}
			mean1 = Math.round(sum1 / n1);
			mean2 = Math.round(sum2 / n2);
			T = (mean1 + mean2) / 2;
		} while (prevMean1 != mean1 && prevMean2 != mean2 );
		
		/* 
		   Update pixel value by using threshold T
		   if pixel values > Threshold (T) then pixel value = 255
		   else pixel values = 0 
		*/
		for (int i = 0; i < mat.rows(); i++) {
			for (int j = 0; j < mat.cols(); j++) {
				double data[] = mat.get(i, j);
				if (data[0] > T) {
					mat.put(i, j, 255);
				}
				else {
					mat.put(i, j, 0);
				}
			}
		}
		
		/* display threshold image and print threshold value (T) */
		Image image2 = convertCVMatToImage(mat);
		thresholdImg.setImage(image2);
		lblThreshold.setText(""+T);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

}
