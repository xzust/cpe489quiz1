package views;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

import javax.swing.GrayFilter;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.highgui.Highgui;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Kanes K. | 56070501004
 *
 */
public class MainViewController implements Initializable {

	/* Variables that bind with FXML Objects*/
	@FXML
	private ImageView imageView;
	@FXML
	private Canvas canvasHistrogram;
	
	/* Convert OpenCV Mat to JavaFX Image Object */
	public Image convertCVMatToImage(Mat mat){
        MatOfByte buffer = new MatOfByte();
        Highgui.imencode(".bmp", mat, buffer);
        return  new Image(new ByteArrayInputStream(buffer.toArray()));
	}
	
	/* Load image from Image Source then display in JavaFX ImageView Object */
	public void loadImage(String imgSrc)
	{
        Mat mat = Highgui.imread(imgSrc, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        
        Image image = convertCVMatToImage(mat);
        imageView.setImage(image);
	}
	
	/* Calculate Histogram from Image Source then plot in JavaFX Canvas Object */
	public void calculateHistogram(String imgSrc)
	{
		int histogramCount[] = new int[256];
		Mat mat = Highgui.imread(imgSrc, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		
		/* Compute Histogram */
		for(int i = 0; i<mat.rows();i++){
			for(int j=0;j<mat.cols();j++){
				double data[] = mat.get(i, j);
				histogramCount[(int)data[0]]++;
			}
		}
		
		/* Scale Histogram (MAX = 300) */
		int max = Arrays.stream(histogramCount).max().getAsInt();
		for(int i=0 ;i<histogramCount.length;i++){
			histogramCount[i] = histogramCount[i]*300/max;	
		}
		
		/* Plot histogram in canvas */
		GraphicsContext gc = canvasHistrogram.getGraphicsContext2D() ;
		for(int i=0; i<256;i++)
			gc.strokeLine(i, 0, i, histogramCount[i]); // Draw Line from x1,y1 to x2,y2
	}


	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
}
