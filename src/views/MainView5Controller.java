package views;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.MatOfByte;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Kanes K. | 56070501004
 *
 */
public class MainView5Controller implements Initializable {

	/* Variables that bind with FXML Objects */
	@FXML
	private ImageView originalImg;
	@FXML
	private ImageView gMaskImg;
	@FXML
	private ImageView gaussian13xImg;

	/* Convert OpenCV Mat to JavaFX Image Object */
	public Image convertCVMatToImage(Mat mat) {
		MatOfByte buffer = new MatOfByte();
		Highgui.imencode(".bmp", mat, buffer);
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}

	/**
	 * Gaussian Filter Method, input three parameters
	 * 
	 * @param imgSrc
	 *            -> Path to image
	 * @param kSize
	 *            -> Kernel Size
	 * @param sigma
	 *            -> Sigma
	 */
	public void gaussianFilter(String imgSrc, int kSize, int sigma) {

		/*
		 * Display Original Image 
		 * Initialize Gaussian Mask Mat and Gaussian Mask Scaled Mat which will used below
		 */
		Mat gMask = new Mat(kSize, kSize, CvType.CV_64FC1);
		Mat gMaskScaled = new Mat(kSize, kSize, CvType.CV_64FC1);
		Mat src = Highgui.imread(imgSrc, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		

		Image image = convertCVMatToImage(src);
		originalImg.setImage(image);

		/*
		 * Calculate g(i,j) Math.E is equal to exponential e
		 */
		int n = kSize / 2;
		for (int i = 0; i < kSize; i++) {
			for (int j = 0; j < kSize; j++) {
				double powCon = (Math.pow(i - n, 2) + Math.pow(j - n, 2)) / (2 * Math.pow(sigma, 2));
				gMask.put(i, j, Math.pow(Math.E, -powCon));
			}
		}

		/*
		 * Calculate G(i,j) -- (ignore 1/C) Divide each g(i,j) with min Get min
		 * of Mat by using OpenCV Method Core.minMaxLoc(Mat).minVal
		 */
		double min = Core.minMaxLoc(gMask).minVal;
		for (int i = 0; i < kSize; i++) {
			for (int j = 0; j < kSize; j++) {
				double[] data = gMask.get(i, j);
				gMask.put(i, j, Math.round(data[0] / min));
			}
		}

		/*
		 * Re-Calculate G(i,j) by divide each value with C Get C or SUM of Mat
		 * by using OpenCV Method Core.sumElems(Mat).val
		 */
		double[] C = Core.sumElems(gMask).val;
		for (int i = 0; i < kSize; i++) {
			for (int j = 0; j < kSize; j++) {
				double[] data = gMask.get(i, j);
				gMask.put(i, j, data[0] / C[0]);
			}
		}

		/*
		 * Scaling Gaussian Mask Mat Value to MAX 255 Display Scaled Gaussian
		 * Mask (Auto Scale to JavaFX ImageView Object from kSize x kSize to
		 * 300x300)
		 */
		double max = Core.minMaxLoc(gMask).maxVal;
		for (int i = 0; i < kSize; i++) {
			for (int j = 0; j < kSize; j++) {
				double[] data = gMask.get(i, j);
				gMaskScaled.put(i, j, data[0] * 255 / max);
			}
		}
		Image image2 = convertCVMatToImage(gMaskScaled);
		gMaskImg.setImage(image2);
		
		// Call convolution function
		Mat desMat = convolution(src, gMask);
		
		/*
		 * Display Gaussian Filterd Image
		 */
		Image image3 = convertCVMatToImage(desMat);
		gaussian13xImg.setImage(image3);
	}

	/**
	 * Convolution Method with two parameters
	 * @param src -> Source as Mat
	 * @param gMask -> Mask that used for convolution (Ex. Gaussian Mask)
	 * @return Result of convolution as Mat
	 */
	private Mat convolution(Mat src, Mat gMask) {
		
		/*
		 * Initialize Destination Mat and Extended Source Mat
		 */
		Mat srcExtended = new Mat();
		Mat desMat = new Mat(src.rows(), src.cols(), CvType.CV_64FC1);
		
		int n = gMask.rows() / 2;
		// Extend border of Source Image save result in srcExtended
		Imgproc.copyMakeBorder(src, srcExtended, n, n, n, n, Imgproc.BORDER_REPLICATE);
		
		/*
		 * Convolution Calculation by for-loop on SOURCE IMAGE but get value
		 * from EXTENDED IMAGE each step in loop > use for-loop again for
		 * multiply Extended image with Mask save result in Destination Mat
		 */
		double conv = 0;
		for (int i = 0; i < src.rows(); i++) {
			for (int j = 0; j < src.cols(); j++) {
				for (int x = 0; x < gMask.rows(); x++) {
					for (int y = 0; y < gMask.cols(); y++) {
						double[] gMaskData = gMask.get(x, y);
						double[] data = srcExtended.get(i + x, j + y);
						conv = conv + (gMaskData[0] * data[0]);
					}
				}
				desMat.put(i, j, conv);
				conv = 0;
			}
		}
		
		return desMat;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

}
