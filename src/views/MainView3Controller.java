package views;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.MatOfByte;
import org.opencv.highgui.Highgui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Kanes K. | 56070501004
 *
 */
public class MainView3Controller implements Initializable {

	/* Variables that bind with FXML Objects */
	@FXML
	private ImageView originalImg;
	@FXML
	private ImageView median5xImg;
	@FXML
	private ImageView median11xImg;
	
	/* Convert OpenCV Mat to JavaFX Image Object */
	public Image convertCVMatToImage(Mat mat) {
		MatOfByte buffer = new MatOfByte();
		Highgui.imencode(".bmp", mat, buffer);
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}

	/* Load image from Image Source then display in JavaFX ImageView Object */
	public void loadImage(String imgSrc) {
		
		/* read original image and display */
		Mat mat = Highgui.imread(imgSrc, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		Image image = convertCVMatToImage(mat);
		originalImg.setImage(image);
		
		/* 
		 * Initialize OpenCV Imgproc Object in order to using Median Filter 
		 * mat2, mat3 as destination of median filter
		 * mat2 using median filter of 5x5
		 * mat3 using median filter of 11x11
		*/
		Imgproc imgproc = new Imgproc();
		Mat mat2 = new Mat();
		Mat mat3 = new Mat();
		imgproc.medianBlur(mat, mat2, 5);
		imgproc.medianBlur(mat, mat3, 11);
		
		/* Convert OpenCV Mat into JavaFX image and display */
		Image image2 = convertCVMatToImage(mat2);
		Image image3 = convertCVMatToImage(mat3);
		median5xImg.setImage(image2);
		median11xImg.setImage(image3);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

}
